import { combineReducers } from 'redux'
import todoReducer from './Todo/reducer'

export default combineReducers({
    todo: todoReducer,
})