import { ADD_TASK, DELETE_TASK, FETCH_TASK, UPDATE_TASK, UPDATE_TASK_STATUS } from "./actionTypes";

const initialState = {
    tasks: [],
}

const todoReducer = (state = initialState, action) => {
    const { type, payload } = action;
    switch (type) {
        case FETCH_TASK:
            return {
                ...state,
                tasks: payload
            }
        case ADD_TASK:
            localStorage.setItem("totoTasks", JSON.stringify([...state.tasks, payload]))
            return {
                ...state,
                tasks: [...state.tasks, payload]
            }

        case UPDATE_TASK_STATUS:
            const index = state.tasks.findIndex(item => item.id === payload)
            let finalData = [...state.tasks]
            finalData[index] = { ...finalData[index], status: !finalData[index].status }

            localStorage.setItem("totoTasks", JSON.stringify(finalData))


            return {
                ...state,
                tasks: finalData
            }
        case UPDATE_TASK:
            const findIindex = state.tasks.findIndex(item => item.id === payload.id)
            let finalDataUpdate = [...state.tasks]
            finalDataUpdate[findIindex] = { ...finalDataUpdate[findIindex], tasks: payload.tasks }

            localStorage.setItem("totoTasks", JSON.stringify(finalDataUpdate))

            return {
                ...state,
                tasks: finalDataUpdate
            }
        case DELETE_TASK:
            const finalTasks = state.tasks.filter(task => task.id !== payload)

            localStorage.setItem("totoTasks", JSON.stringify(finalTasks))

            return {
                ...state,
                tasks: finalTasks
            }
        default:
            return state
    }
}

export default todoReducer