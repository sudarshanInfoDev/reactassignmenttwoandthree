import { ADD_TASK, DELETE_TASK, UPDATE_TASK, UPDATE_TASK_STATUS, FETCH_TASK } from "./actionTypes"

export const fetchDataFromLocalStorage = (data) => dispatch => {
    dispatch({
        type: FETCH_TASK,
        payload: data
    })
}

export const addTasks = (data) => dispatch => {
    dispatch({
        type: ADD_TASK,
        payload: data
    })
}

export const updateTasks = (data) => dispatch => {
    dispatch({
        type: UPDATE_TASK,
        payload: data
    })
}

export const updateTaskStatus = (id) => dispatch => {
    dispatch({
        type: UPDATE_TASK_STATUS,
        payload: id
    })
}


export const deleteTasks = (id) => dispatch => {
    dispatch({
        type: DELETE_TASK,
        payload: id
    })
}