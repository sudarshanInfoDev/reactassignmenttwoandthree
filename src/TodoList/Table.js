import React from 'react'

const Table = ({
    tasks,
    handleTaskStatusChange,
    handleTaskDelete,
    handleTaskEdit
}) => {


    return (
        <>
            <table className="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Tasks</th>
                        <th scope="col" className="text-center">Completed</th>
                        <th scope="col" className="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        tasks.length > 0 ?
                            tasks.map((task, index) => (
                                <tr key={task.id}>
                                    <th scope="row"><span></span>{index + 1}</th>
                                    <td className="w-custom">
                                        <span className={`${task.status ? 'cross-task' : 'font-weight-bold'}`}>
                                            {task.tasks}
                                        </span>
                                    </td>
                                    <td className="text-center">
                                        <input
                                            type="checkbox"
                                            checked={task.status}
                                            onChange={() => handleTaskStatusChange(task.id)}
                                        />
                                    </td>
                                    <td className="text-center">
                                        <button
                                            className="btn btn-sm mx-1 btn-outline-primary"
                                            disabled={task.status}
                                            style={{
                                                cursor: `${task.status ? 'not-allowed' : "pointer"}`
                                            }}
                                            onClick={() => handleTaskEdit(task.id)}
                                        >
                                            Edit
                                        </button>
                                        <button
                                            className="btn btn-sm mx-1 btn-outline-danger"
                                            onClick={() => handleTaskDelete(task.id)}
                                        >
                                            Delete
                                        </button>

                                    </td>
                                </tr>
                            ))
                            :
                            <tr>
                                <td colSpan={4} className="text-center font-weight-bold">No Datas Available</td>
                            </tr>
                    }

                </tbody>
            </table>
        </>
    )
}

export default Table
