import { useFormik } from 'formik';
import React from 'react';
import * as Yup from 'yup';


const FormComponent = ({ initialState, handleTaskSubmit }) => {

    const { handleSubmit, handleChange, handleBlur, values, touched, errors } = useFormik({
        enableReinitialize: true,
        initialValues: initialState,
        validationSchema: Yup.object({
            tasks: Yup.string().required("Task is required."),

        }),
        onSubmit: (values, { resetForm }) => {
            handleTaskSubmit(values, resetForm)
        }
    })


    return (
        <>

            <form onSubmit={handleSubmit}>
                <div className="row align-items-center">
                    <div className="col-10">
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Add Task"
                            name="tasks"
                            value={values.tasks}
                            onChange={handleChange}
                            onBlur={handleBlur}
                        />
                        {
                            errors.tasks && touched.tasks &&
                            <small className="text-danger font-weight-bold mt-2">{errors.tasks}</small>
                        }

                    </div>
                    <div className="col-2">
                        <button className="btn  mx-1 btn-outline-success" type="submit" >{values.id ? "Update" : "Add"} Task</button>
                    </div>

                </div>
            </form>

        </>
    )
}

export default FormComponent
