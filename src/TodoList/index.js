import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { addTasks, deleteTasks, fetchDataFromLocalStorage, updateTasks, updateTaskStatus } from '../Store/Todo/actions'
import Form from './Form'
import Table from './Table'

const TodoList = () => {

    const [initialState, setInitialState] = useState({
        id: "",
        tasks: ""
    })


    const dispatch = useDispatch()
    const tasks = useSelector(state => state.todo.tasks)

    useEffect(() => {
        const data = localStorage.getItem("totoTasks")
        if (data) {
            dispatch(fetchDataFromLocalStorage(JSON.parse(data)))
        }
    }, [])


    const handleTaskSubmit = (data, resetForm) => {
        console.log("subm,itted")
        if (data.id) {
            dispatch(updateTasks(data))
            setInitialState({ tasks: "" })

        } else {
            const finalData = {
                tasks: data.tasks,
                id: Math.random(),
                status: false
            }
            dispatch(addTasks(finalData))
            resetForm()
        }
    }

    const handleTaskStatusChange = (id) => {
        dispatch(updateTaskStatus(id))
    }

    const handleTaskDelete = (id) => {
        dispatch(deleteTasks(id))
    }
    const handleTaskEdit = (id) => {
        const taskData = tasks.find(item => item.id === id)
        setInitialState(taskData)
    }

    return (
        <div className="row mx-5 my-3 ">
            <div className="col-12 mb-3">
                {/* <h2 className="font-weight-bold text-center">TODO </h2> */}
                <h5 className="font-weight-bold text-center">TODO Form</h5>
            </div>
            <div className="col-12">
                <Form
                    initialState={initialState}
                    handleTaskSubmit={handleTaskSubmit}

                />
            </div>
            <div className="col-12 mt-4">
                <h5 className="font-weight-bold text-center">TODO List</h5>
                <Table
                    tasks={tasks}
                    handleTaskStatusChange={handleTaskStatusChange}
                    handleTaskDelete={handleTaskDelete}
                    handleTaskEdit={handleTaskEdit}
                />
            </div>
        </div>
    )
}

export default TodoList
