import React from 'react'
import './App.css'
import TodoList from './TodoList'
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import FormikAssignment from './FormikAssignment';
import Navbar from './Component/Navbar';

const App = () => {
  return (
    <>
      <BrowserRouter>
        <Navbar />
        <Switch>
          <Route exact path="/todo" component={TodoList}></Route>
          <Route exact path="/formik" component={FormikAssignment}></Route>
          <Redirect from="/" to="/todo" />
        </Switch>
      </BrowserRouter>
    </>
  )
}

export default App
